Cadena str;
Boton b1,b2,b3,b4,b5,b6,b7;
PFont p;
String men,men1,men2,men3,men4,men5,men6;
public void setup(){
    size(500,400);
    smooth();
    str = new Cadena("mexico");
    p = createFont("Arial",18);
    textFont(p);
    
    b1 = new Boton(20,25,"Invierte");
    men1 ="Aqui va el mensaje";
    
    b2 = new Boton(120,25,"BorrarI");
    men2 ="Aqui va el mensaje";
     
     b3 = new Boton(220,25,"Vaciar");
     men3 ="Aqui va el mensaje";
      
     b4 = new Boton(20,75,"AgregarF");
     men4 ="Aqui va el mensaje";
       
     b5 = new Boton(120,75,"AgregarI");
     men5 ="Aqui va el mensaje";
        
     b6 = new Boton(220,75,"Llenar");
     men6 ="Aqui va el mensaje";
         
    b7 = new Boton(20,120,"BorrarF");
    men ="Aqui va el mensaje";
}

public void draw(){
     background(127);
     fill(0,255,0);
     text(str.toString(),20,height/2);
     b1.dibujar();
     fill(255);
     text(men1,20,height-30);
     b2.dibujar();
     fill(255);
     text(men2,20,height-30);
     b3.dibujar();
     fill(255);
     text(men3,20,height-30);
     b4.dibujar();
     fill(255);
     text(men4,20,height-30);
     b5.dibujar();
     fill(255);
     text(men5,20,height-30); 
     b6.dibujar();
     fill(255);
     text(men6,20,height-30);
     b7.dibujar();
     fill(255);
     text(men,20,height-30);
}

public void mousePressed(){
    men1 = b1.click(mouseX, mouseY);
    men2 = b2.click(mouseX, mouseY);
    men3 = b3.click(mouseX, mouseY);
    men4 = b4.click(mouseX, mouseY);
    men5 = b5.click(mouseX, mouseY);
    men6 = b6.click(mouseX, mouseY);
    men = b7.click(mouseX, mouseY);
    
}